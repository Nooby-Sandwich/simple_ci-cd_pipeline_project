module "vpc" {
  source = "./vpc"
}

module "my_sg" {
  source = "./ec2"
  sn     = module.vpc.pb_sn
  sg     = module.vpc.my_sg
}