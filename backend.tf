terraform {
  backend "s3" {
    bucket = "ci-cd-terraform-project-bucket"
    key = "state"
    region = "us-east-1"
    dynamodb_table = "backend-project-table"
  }
}