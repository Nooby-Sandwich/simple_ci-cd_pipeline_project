output "pb_sn" {
  value = aws_subnet.pb_sn.id
}

output "my_sg" {
  value = aws_security_group.my_sg.id
}
